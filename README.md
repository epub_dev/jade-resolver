# Jade Resolver

A generic module that searches for files in a specified list of directories, with glob support.

## Installation

```sh
npm install @epublishing/jade-resolver
```

## Usage

See the [API documentation](./doc/API.md).

_Copyright &copy; 2017, ePublishing, Inc._
