<a name="Resolver"></a>

## Resolver
Finds files by glob pattern across Jade, child gem, and site directories

**Kind**: global class  

* [Resolver](#Resolver)
    * [new Resolver([pathsObject], [options])](#new_Resolver_new)
    * [.addPath(name, path)](#Resolver+addPath) ⇒ <code>Object</code>
    * [.removePath(name)](#Resolver+removePath) ⇒ <code>Object</code>
    * [.find(pattern)](#Resolver+find) ⇒ <code>Array.&lt;string&gt;</code>
    * [.findIn(pattern, pathName)](#Resolver+findIn) ⇒ <code>Array.&lt;string&gt;</code>
    * [.exists(pattern)](#Resolver+exists) ⇒ <code>boolean</code>
    * [.existsIn(pattern, pathName)](#Resolver+existsIn) ⇒ <code>boolean</code>
    * [.readFile(pattern)](#Resolver+readFile) ⇒ <code>Promise.&lt;string&gt;</code>
    * [.readFileSync(pattern)](#Resolver+readFileSync) ⇒ <code>string</code>
    * [.resolve(pattern)](#Resolver+resolve) ⇒ <code>string</code>

<a name="new_Resolver_new"></a>

### new Resolver([pathsObject], [options])
Create a new instance of Resolver


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| [pathsObject] | <code>Object.&lt;string, string&gt;</code> | <code>{}</code> | A string-keyed object of named paths |
| [options] | <code>Object.&lt;string, \*&gt;</code> | <code>{}</code> | Optional configuration parameters |
| [options.includeSite] | <code>boolean</code> | <code>true</code> | Include the current working directory in resolution paths |

**Example** *(Create a resolver inside a Grunt task)*  
```js
const resolver = new Resolver(grunt.config.get('paths'));
// => Resolver({ jade: '/home/foo/work/jade', jadechild: '/home/foo/work/jadechild', site: '/home/foo/work/site' })
```
<a name="Resolver+addPath"></a>

### resolver.addPath(name, path) ⇒ <code>Object</code>
Add a directory to the resolver's search path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>Object</code> - The updated search path  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The name of the path (i.e. "jade") |
| path | <code>string</code> | An absolute path string |

**Example**  
```js
resolver.addPath('childgem', '/home/foo/work/childgem')
// => { jade: '/home/foo/work/jade', childgem: '/home/foo/work/childgem', site: '/home/foo/work/site' }
```
<a name="Resolver+removePath"></a>

### resolver.removePath(name) ⇒ <code>Object</code>
Remove a named directory from the resolver's search path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>Object</code> - The updated search path  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The path's key name |

<a name="Resolver+find"></a>

### resolver.find(pattern) ⇒ <code>Array.&lt;string&gt;</code>
Find files matching the specified pattern within the search path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of matching path names  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | A filename or glob pattern to search for |

**Example** *(Search for a literal file name)*  
```js
let results = resolver.find('package.json');
// => [ '/home/foo/work/site/package.json' ]
```
**Example** *(Search for files matching a specific pattern)*  
```js
let results = resolver.find('*.json');
// => [ '/home/foo/work/site/package.json', '/home/foo/work/site/bower.json' ]
```
<a name="Resolver+findIn"></a>

### resolver.findIn(pattern, pathName) ⇒ <code>Array.&lt;string&gt;</code>
Search for files matching the specified pattern, but only in the named path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>Array.&lt;string&gt;</code> - An array of matching path names  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | A filename or glob pattern to search for |
| pathName | <code>string</code> | The name of the path in which to search |

**Example** *(Find JSON files at the root of the &#x27;site&#x27; path)*  
```js
let results = resolver.findIn('*.json', 'site');
// => [ '/home/foo/work/site/package.json' ]
```
<a name="Resolver+exists"></a>

### resolver.exists(pattern) ⇒ <code>boolean</code>
Check whether a matching file exists, anywhere in the search path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>boolean</code> - Whether or not the file exists  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | The pattern to search for |

<a name="Resolver+existsIn"></a>

### resolver.existsIn(pattern, pathName) ⇒ <code>boolean</code>
Same as `exists`, but restricted to the named path

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>boolean</code> - Whether or not the file exists  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | The pattern to search for |
| pathName | <code>string</code> | The path in which to search |

<a name="Resolver+readFile"></a>

### resolver.readFile(pattern) ⇒ <code>Promise.&lt;string&gt;</code>
Asynchronously read the first file that matches the specified pattern

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>Promise.&lt;string&gt;</code> - A Promise that resolves to the file's contents, as a UTF-8 string  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | The pattern to search for |

<a name="Resolver+readFileSync"></a>

### resolver.readFileSync(pattern) ⇒ <code>string</code>
Synchronously read the first file matching the specified pattern

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>string</code> - The file's contents as a UTF-8 string  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | The pattern to search for |

<a name="Resolver+resolve"></a>

### resolver.resolve(pattern) ⇒ <code>string</code>
Return the first result found for the specified pattern

**Kind**: instance method of [<code>Resolver</code>](#Resolver)  
**Returns**: <code>string</code> - The first result matching the pattern  

| Param | Type | Description |
| --- | --- | --- |
| pattern | <code>string</code> | The pattern to search for |

