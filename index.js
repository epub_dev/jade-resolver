const _            = require('lodash');
const path         = require('path');
const globby       = require('globby');
const fs           = require('fs');
const EventEmitter = require('events');

/**
 * Finds files by glob pattern across Jade, child gem, and site directories
 */
class Resolver extends EventEmitter {
  static get defaults() {
    return {
      includeSite: true,
    };
  }

  /**
   * Create a new instance of Resolver
   * @param {Object.<string,string>} [pathsObject={}] - A string-keyed object of named paths
   * @param {Object.<string,*>} [options={}] - Optional configuration parameters
   * @param {boolean} [options.includeSite=true] - Include the current working directory in resolution paths
   * @example <caption>Create a resolver inside a Grunt task</caption>
   * const resolver = new Resolver(grunt.config.get('paths'));
   * // => Resolver({ jade: '/home/foo/work/jade', jadechild: '/home/foo/work/jadechild', site: '/home/foo/work/site' })
   */
  constructor(pathsObject = {}, options = {}) {
    super();
    this.options   = Object.assign({}, this.constructor.defaults, options);
    this.pathNames = pathsObject;
    if (this.options.includeSite) this.pathNames.site = process.cwd();
    this.on('update', this._makeSearchPath.bind(this));
    this._makeSearchPath();
  }

  /**
   * Add a directory to the resolver's search path
   * @param {string} name - The name of the path (i.e. "jade")
   * @param {string} path - An absolute path string
   * @example
   * resolver.addPath('childgem', '/home/foo/work/childgem')
   * // => { jade: '/home/foo/work/jade', childgem: '/home/foo/work/childgem', site: '/home/foo/work/site' }
   * @returns {Object} The updated search path
   */
  addPath(name, path) {
    this.pathsNames[name] = path;
    this.emit('update');
    return this.pathNames;
  }

  /**
   * Remove a named directory from the resolver's search path
   * @param {string} name - The path's key name
   * @returns {Object} The updated search path
   */
  removePath(name) {
    delete this.pathNames[name];
    this.emit('update');
    return this.pathNames;
  }

  /**
   * Find files matching the specified pattern within the search path
   * @param {string} pattern - A filename or glob pattern to search for
   * @example <caption>Search for a literal file name</caption>
   * let results = resolver.find('package.json');
   * // => [ '/home/foo/work/site/package.json' ]
   * @example <caption>Search for files matching a specific pattern</caption>
   * let results = resolver.find('*.json');
   * // => [ '/home/foo/work/site/package.json', '/home/foo/work/site/bower.json' ]
   * @returns {string[]} An array of matching path names
   */
  find(pattern) {
    if (!pattern) {
      pattern = '';
    }

    const globPaths = this._globPaths(pattern);
    return globby.sync(globPaths);
  }

  /**
   * Search for files matching the specified pattern, but only in the named path
   * @param {string} pattern - A filename or glob pattern to search for
   * @param {string} pathName - The name of the path in which to search
   * @example <caption>Find JSON files at the root of the 'site' path</caption>
   * let results = resolver.findIn('*.json', 'site');
   * // => [ '/home/foo/work/site/package.json' ]
   * @returns {string[]} An array of matching path names
   */
  findIn(pattern, pathName) {
    if (!pattern) {
      pattern = '';
    }

    if (!pathName) {
      pathName = 'site';
    }

    const fullGlob = path.join(this.pathNames[pathName], pattern);
    return globby.sync(fullGlob);
  }

  /**
   * Check whether a matching file exists, anywhere in the search path
   * @param {string} pattern - The pattern to search for
   * @returns {boolean} Whether or not the file exists
   */
  exists(pattern) {
    return !_.isEmpty(this.find(pattern));
  }

  /**
   * Same as `exists`, but restricted to the named path
   * @param {string} pattern - The pattern to search for
   * @param {string} pathName - The path in which to search
   * @returns {boolean} Whether or not the file exists
   */
  existsIn(pattern, pathName) {
    return !_.isEmpty(this.findIn(pattern, pathName));
  }

  /**
   * Asynchronously read the first file that matches the specified pattern
   * @param {string} pattern - The pattern to search for
   * @returns {Promise.<string>} A Promise that resolves to the file's contents, as a UTF-8 string
   */
  readFile(pattern) {
    return new Promise((resolve, reject) => {
      const results = this.find(pattern);

      if (!results.length) return reject(new Error('File not found'));

      fs.readFile(results[0], function(err, data) {
        if (err) return reject(err);
        resolve(data.toString());
      });
    });
  }

  /**
   * Synchronously read the first file matching the specified pattern
   * @param {string} pattern - The pattern to search for
   * @returns {string} The file's contents as a UTF-8 string
   */
  readFileSync(pattern) {
    const results = this.find(pattern);

    if (!results.length) return null;

    return fs.readFileSync(results[0]).toString();
  }

  /**
   * Return the first result found for the specified pattern
   * @param {string} pattern - The pattern to search for
   * @returns {string} The first result matching the pattern
   */
  resolve(pattern) {
    return this.find(pattern)[0];
  }

  _globPaths(pattern) {
    return this.searchPath.map((base) => path.join(base, pattern));
  }

  _makeSearchPath() {
    this.searchPath = Object.values(this.pathNames).filter((val) => typeof val === 'string');
  }
}

module.exports = Resolver;
